package com.xujiu.caigoumall;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.wxpay.sdk.WXPay;
import com.xujiu.caigoumall.config.MyPayConfig;
import com.xujiu.caigoumall.system.entity.ShoppingCart;
import com.xujiu.caigoumall.system.entity.vo.OrdersVO;
import com.xujiu.caigoumall.system.entity.vo.ShoppingCartVO;
import com.xujiu.caigoumall.system.mapper.CategoryMapper;
import com.xujiu.caigoumall.system.mapper.OrdersMapper;
import com.xujiu.caigoumall.system.mapper.ShoppingCartMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class CaigoumallApplicationTests {

    @Test
    void contextLoads() {

    }

}
