package com.xujiu.caigoumall.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@Configuration
@EnableSwagger2WebMvc
public class SwaggerConfig {

    /*swagger会帮助我们生成接口文档
    * 1：配置生成的文档信息
    * 2: 配置生成规则*/

    //200 状态码 ：正确响应
    //401 状态码 ：错误响应
    //zhangsan 666666

    /*Docket封装接口文档信息*/
    @Bean
    public Docket getDocket(){

        Docket docket=new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .title("《菜狗商城》后端接口说明")
                        .description("此文档详细说明了菜狗商城项目后端接口规范")
                        .termsOfServiceUrl("http://localhost:8084/doc.html")
                        .contact("928616479@qq.com")
                        .version("1.0")
                        .build())
                //分组名称
                .groupName("2.0版本")
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.xujiu.caigoumall.system.controller"))
                .paths(PathSelectors.any())
                .build();
        return docket;
    }

}
