package com.xujiu.caigoumall.system.mapper;

import com.xujiu.caigoumall.system.entity.IndexImg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 轮播图  Mapper 接口
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Repository
public interface IndexImgMapper extends BaseMapper<IndexImg> {

}
