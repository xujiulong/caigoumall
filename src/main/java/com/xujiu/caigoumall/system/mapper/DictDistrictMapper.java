package com.xujiu.caigoumall.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xujiu.caigoumall.system.entity.DictDistrict;
import org.springframework.stereotype.Repository;

/**
 *  地区字典 Mapper 接口
 *
 * @author 许久龙
 * @since 2022-02-27
 */
@Repository
public interface DictDistrictMapper extends BaseMapper<DictDistrict> {

}
