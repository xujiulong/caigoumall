package com.xujiu.caigoumall.system.mapper;

import com.xujiu.caigoumall.system.entity.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xujiu.caigoumall.system.entity.vo.ProductVO;
import org.springframework.stereotype.Repository;

/**
 * 商品 商品信息相关表：分类表，商品图片表，商品规格表，商品参数表 Mapper 接口
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Repository
public interface ProductMapper extends BaseMapper<ProductVO> {

}
