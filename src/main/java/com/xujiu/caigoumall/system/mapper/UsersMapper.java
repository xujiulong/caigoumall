package com.xujiu.caigoumall.system.mapper;

import com.xujiu.caigoumall.system.entity.Users;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.springframework.stereotype.Repository;

/**
 * 用户  Mapper 接口
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Repository
public interface UsersMapper extends BaseMapper<Users> {

}
