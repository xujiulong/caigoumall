package com.xujiu.caigoumall.system.mapper;

import com.xujiu.caigoumall.system.entity.OrderItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 订单项/快照  Mapper 接口
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Repository
public interface OrderItemMapper extends BaseMapper<OrderItem> {

}
