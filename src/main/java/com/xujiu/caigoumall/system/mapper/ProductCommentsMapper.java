package com.xujiu.caigoumall.system.mapper;

import com.xujiu.caigoumall.system.entity.ProductComments;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xujiu.caigoumall.system.entity.vo.ProductCommentsVO;
import org.springframework.stereotype.Repository;

/**
 * 商品评价  Mapper 接口
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Repository
public interface ProductCommentsMapper extends BaseMapper<ProductCommentsVO> {

}
