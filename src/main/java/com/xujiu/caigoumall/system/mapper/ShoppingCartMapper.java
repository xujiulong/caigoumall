package com.xujiu.caigoumall.system.mapper;

import com.xujiu.caigoumall.system.entity.ShoppingCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xujiu.caigoumall.system.entity.vo.ShoppingCartVO;
import org.springframework.stereotype.Repository;

/**
 * 购物车  Mapper 接口
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Repository
public interface ShoppingCartMapper extends BaseMapper<ShoppingCartVO> {

}
