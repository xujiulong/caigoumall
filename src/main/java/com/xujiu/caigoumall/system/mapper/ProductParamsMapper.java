package com.xujiu.caigoumall.system.mapper;

import com.xujiu.caigoumall.system.entity.ProductParams;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 商品参数  Mapper 接口
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Repository
public interface ProductParamsMapper extends BaseMapper<ProductParams> {

}
