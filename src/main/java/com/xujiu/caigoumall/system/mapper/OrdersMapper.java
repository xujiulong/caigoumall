package com.xujiu.caigoumall.system.mapper;

import com.xujiu.caigoumall.system.entity.Orders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xujiu.caigoumall.system.entity.vo.OrdersVO;
import org.springframework.stereotype.Repository;

/**
 * 订单  Mapper 接口
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Repository
public interface OrdersMapper extends BaseMapper<OrdersVO> {

}
