package com.xujiu.caigoumall.system.mapper;

import com.xujiu.caigoumall.system.entity.UserAddr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 用户地址  Mapper 接口
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Repository
public interface UserAddrMapper extends BaseMapper<UserAddr> {

}
