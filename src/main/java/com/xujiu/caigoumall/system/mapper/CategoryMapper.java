package com.xujiu.caigoumall.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xujiu.caigoumall.system.entity.vo.CategoryVO;
import org.springframework.stereotype.Repository;

/**
 * 商品分类 Mapper 接口
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Repository
public interface CategoryMapper extends BaseMapper<CategoryVO> {

}
