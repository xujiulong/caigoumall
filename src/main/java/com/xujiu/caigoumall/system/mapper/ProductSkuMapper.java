package com.xujiu.caigoumall.system.mapper;

import com.xujiu.caigoumall.system.entity.ProductSku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 商品规格 每一件商品都有不同的规格，不同的规格又有不同的价格和优惠力度，规格表为此设计 Mapper 接口
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Repository
public interface ProductSkuMapper extends BaseMapper<ProductSku> {

}
