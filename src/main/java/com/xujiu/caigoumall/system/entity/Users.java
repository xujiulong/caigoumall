package com.xujiu.caigoumall.system.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @author 许久龙
 * @since 2022-02-21
 */
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_users")
@ApiModel(value="Users对象", description="用户 ")
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id ")
    @TableId(value = "user_id",type = IdType.AUTO)
    private Integer userId;

    @ApiModelProperty(value = "用户名 ")
    @TableField("username")
    private String username;

    @ApiModelProperty(value = "密码")
    @TableField("password")
    private String password;

    @ApiModelProperty(value = "昵称")
    @TableField("nickname")
    private String nickname;

    @ApiModelProperty(value = "真实姓名")
    @TableField("realname")
    private String realname;

    @ApiModelProperty(value = "头像")
    @TableField("user_img")
    private String userImg;

    @ApiModelProperty(value = "手机号 手机号")
    @TableField("user_mobile")
    private String userMobile;

    @ApiModelProperty(value = "邮箱地址")
    @TableField("user_email")
    private String userEmail;

    @ApiModelProperty(value = "性别 M(男) or F(女)")
    @TableField("user_sex")
    private String userSex;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
