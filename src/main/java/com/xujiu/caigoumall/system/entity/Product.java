package com.xujiu.caigoumall.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @author 许久龙
 * @since 2022-02-21
 */
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_product")
@ApiModel(value="Product对象", description="商品 商品信息相关表：分类表，商品图片表，商品规格表，商品参数表")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商品主键id")
    @TableId("product_id")
    private String productId;

    @ApiModelProperty(value = "商品名称 商品名称")
    @TableField("product_name")
    private String productName;

    @ApiModelProperty(value = "分类外键id 分类id")
    @TableField("category_id")
    private Integer categoryId;

    @ApiModelProperty(value = "一级分类外键id 一级分类id，用于优化查询")
    @TableField("root_category_id")
    private Integer rootCategoryId;

    @ApiModelProperty(value = "销量 累计销售")
    @TableField("sold_num")
    private Integer soldNum;

    @ApiModelProperty(value = "默认是1，表示正常状态, -1表示删除, 0下架 默认是1，表示正常状态, -1表示删除, 0下架")
    @TableField("product_status")
    private Integer productStatus;

    @ApiModelProperty(value = "商品内容 商品内容")
    @TableField("content")
    private String content;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
