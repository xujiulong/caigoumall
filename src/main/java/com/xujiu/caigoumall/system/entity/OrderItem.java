package com.xujiu.caigoumall.system.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import java.math.BigInteger;
import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @author 许久龙
 * @since 2022-02-21
 */
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_order_item")
@ApiModel(value="OrderItem对象", description="订单项/快照 ")
public class OrderItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单项ID")
    @TableId(value = "item_id")
    private String itemId;

    @ApiModelProperty(value = "订单ID")
    @TableField("order_id")
    private String orderId;

    @ApiModelProperty(value = "商品ID")
    @TableField("product_id")
    private String productId;

    @ApiModelProperty(value = "商品名称")
    @TableField("product_name")
    private String productName;

    @ApiModelProperty(value = "商品图片")
    @TableField("product_img")
    private String productImg;

    @ApiModelProperty(value = "skuID")
    @TableField("sku_id")
    private String skuId;

    @ApiModelProperty(value = "sku名称")
    @TableField("sku_name")
    private String skuName;

    @ApiModelProperty(value = "商品价格")
    @TableField("product_price")
    private BigDecimal productPrice;

    @ApiModelProperty(value = "购买数量")
    @TableField("buy_counts")
    private Integer buyCounts;

    @ApiModelProperty(value = "商品总金额")
    @TableField("total_amount")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "加入购物车时间")
    @TableField("basket_date")
    private Date basketDate;

    @ApiModelProperty(value = "评论状态： 0 未评价  1 已评价")
    @TableField("is_comment")
    private Integer isComment;

    @ApiModelProperty(value = "购买时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
