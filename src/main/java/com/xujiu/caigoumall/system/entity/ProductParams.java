package com.xujiu.caigoumall.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @author 许久龙
 * @since 2022-02-21
 */
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_product_params")
@ApiModel(value="ProductParams对象", description="商品参数 ")
public class ProductParams implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商品参数id")
    @TableId("param_id")
    private String paramId;

    @ApiModelProperty(value = "商品id")
    @TableField("product_id")
    private String productId;

    @ApiModelProperty(value = "产地，例：中国江苏")
    @TableField("product_place")
    private String productPlace;

    @ApiModelProperty(value = "保质期，例：180天")
    @TableField("foot_period")
    private String footPeriod;

    @ApiModelProperty(value = "品牌名，例：三只大灰狼")
    @TableField("brand")
    private String brand;

    @ApiModelProperty(value = "生产厂名，例：大灰狼工厂")
    @TableField("factory_name")
    private String factoryName;

    @ApiModelProperty(value = "生产厂址，例：大灰狼生产基地")
    @TableField("factory_address")
    private String factoryAddress;

    @ApiModelProperty(value = "包装方式，例：袋装")
    @TableField("packaging_method")
    private String packagingMethod;

    @ApiModelProperty(value = "规格重量，例：35g")
    @TableField("weight")
    private String weight;

    @ApiModelProperty(value = "存储方法，例：常温5~25°")
    @TableField("storage_method")
    private String storageMethod;

    @ApiModelProperty(value = "食用方式，例：开袋即食")
    @TableField("eat_method")
    private String eatMethod;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
