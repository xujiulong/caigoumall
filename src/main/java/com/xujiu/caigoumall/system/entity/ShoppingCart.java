package com.xujiu.caigoumall.system.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @author 许久龙
 * @since 2022-02-21
 */
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_shopping_cart")
@ApiModel(value="ShoppingCart对象", description="购物车 ")
public class ShoppingCart implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId("cart_id")
    private int cartId;

    @ApiModelProperty(value = "商品ID")
    @TableField("product_id")
    private String productId;

    @ApiModelProperty(value = "skuID")
    @TableField("sku_id")
    private String skuId;

    @ApiModelProperty(value = "用户ID")
    @TableField("user_id")
    private Integer userId;

    @ApiModelProperty(value = "购物车商品数量")
    @TableField("cart_num")
    private String cartNum;

    @ApiModelProperty(value = "添加购物车时商品价格")
    @TableField("product_price")
    private BigDecimal productPrice;

    @ApiModelProperty(value = "选择的套餐的属性")
    @TableField("sku_props")
    private String skuProps;

    @ApiModelProperty(value = "添加购物车时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
