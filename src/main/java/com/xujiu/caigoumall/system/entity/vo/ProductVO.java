package com.xujiu.caigoumall.system.entity.vo;

import com.baomidou.mybatisplus.annotation.*;
import com.xujiu.caigoumall.system.entity.ProductImg;
import com.xujiu.caigoumall.system.entity.ProductSku;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.naming.Name;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_product")
@ApiModel(value="Product对象", description="商品 商品信息相关表：分类表，商品图片表，商品规格表，商品参数表")
public class ProductVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商品主键id")
    @TableId(value="product_id",type = IdType.AUTO)
    private String productId;

    @ApiModelProperty(value = "商品名称")
    @TableField("product_name")
    private String productName;

    @ApiModelProperty(value = "分类外键id 分类id")
    @TableField("category_id")
    private Integer categoryId;

    @ApiModelProperty(value = "一级分类外键id 一级分类id，用于优化查询")
    @TableField("root_category_id")
    private Integer rootCategoryId;

    @ApiModelProperty(value = "销量 累计销售")
    @TableField("sold_num")
    private Integer soldNum;

    @ApiModelProperty(value = "默认是1，表示正常状态, -1表示删除, 0下架 默认是1，表示正常状态, -1表示删除, 0下架")
    @TableField("product_status")
    private Integer productStatus;

    @ApiModelProperty(value = "商品内容")
    @TableField("content")
    private String content;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    //在查询商品的时候，关联查询商品图片信息
    @ApiModelProperty(value = "商品图片")
    @TableField(exist = false)
    private List<ProductImg> imgs;

    //在查询商品的时候，关联查询商品套餐信息
    @ApiModelProperty(value = "商品套餐")
    @TableField(exist = false)
    private List<ProductSku> skus;
}
