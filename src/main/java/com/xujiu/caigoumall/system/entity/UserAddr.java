package com.xujiu.caigoumall.system.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @author 许久龙
 * @since 2022-02-21
 */
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_user_addr")
@ApiModel(value="UserAddr对象", description="用户地址 ")
public class UserAddr implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "地址主键id")
    @TableId(value = "addr_id", type = IdType.AUTO)
    private Integer addrId;

    @ApiModelProperty(value = "用户ID")
    @TableField("user_id")
    private Integer userId;

    @ApiModelProperty(value = "收件人姓名")
    @TableField("receiver_name")
    private String receiverName;

    @ApiModelProperty(value = "收件人手机号")
    @TableField("receiver_mobile")
    private String receiverMobile;

    @ApiModelProperty(value = "邮编")
    @TableField("post_code")
    private String postCode;

    @ApiModelProperty(value = "省-名称")
    @TableField("province_name")
    private String provinceName;

    @ApiModelProperty(value = "省-行政代号")
    @TableField("province_code")
    private String provinceCode;

    @ApiModelProperty(value = "市-名称")
    @TableField("city_name")
    private String cityName;

    @ApiModelProperty(value = "市-行政代号")
    @TableField("city_code")
    private String cityCode;

    @ApiModelProperty(value = "区-名称")
    @TableField("area_name")
    private String areaName;

    @ApiModelProperty(value = "区-行政代号")
    @TableField("area_code")
    private String areaCode;

    @ApiModelProperty(value = "详细地址")
    @TableField("address")
    private String address;

    @ApiModelProperty(value = "状态,1正常，0无效")
    @TableField("status")
    private Integer status;

    @ApiModelProperty(value = "是否默认地址 1是 1:是  0:否")
    @TableField("common_addr")
    private Integer commonAddr;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
