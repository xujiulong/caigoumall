package com.xujiu.caigoumall.system.entity.vo;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 新增 productName、productImg
 */
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_shopping_cart")
@ApiModel(value="ShoppingCart对象", description="购物车 ")
public class ShoppingCartVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "cart_id",type = IdType.AUTO)
    private Integer cartId;

    @ApiModelProperty(value = "商品ID")
    @TableField("product_id")
    private String productId;

    @ApiModelProperty(value = "skuID")
    @TableField("sku_id")
    private String skuId;

    @ApiModelProperty(value = "用户ID")
    @TableField("user_id")
    private Integer userId;

    @ApiModelProperty(value = "购物车商品数量")
    @TableField("cart_num")
    private String cartNum;

    @ApiModelProperty(value = "添加购物车时商品价格")
    @TableField("product_price")
    private BigDecimal productPrice;

    @ApiModelProperty(value = "选择的套餐的属性")
    @TableField("sku_props")
    private String skuProps;

    @ApiModelProperty(value = "添加购物车时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    //根据product_id关联ProductVO查询字段
    @ApiModelProperty(value = "商品名称")
    @TableField(value = "product_name",exist = false)
    private String productName;
    @ApiModelProperty(value = "商品主图片")
    @TableField(exist = false)
    private String productImg;

    //根据sku_id关联ProductSku查询字段
    @ApiModelProperty(value = "原价")
    @TableField(value = "original_price",exist = false)
    private double originalPrice;
    @ApiModelProperty(value = "销售价格")
    @TableField(value = "sell_price",exist = false)
    private double sellPrice;
    @ApiModelProperty(value = "套餐名称")
    @TableField(value = "sku_name",exist = false)
    private String skuName;
    @ApiModelProperty(value = "库存")
    @TableField(value = "stock",exist = false)
    private int skuStock;
}

