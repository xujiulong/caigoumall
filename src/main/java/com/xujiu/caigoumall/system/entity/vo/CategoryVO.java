package com.xujiu.caigoumall.system.entity.vo;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_category")
@ApiModel(value="Category对象", description="商品分类")
public class CategoryVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键 分类id主键")
    @TableId(value = "category_id", type = IdType.AUTO)
    private Integer categoryId;

    @ApiModelProperty(value = "分类名称 分类名称")
    @TableField("category_name")
    private String categoryName;

    @ApiModelProperty(value = "分类层级 ")
    @TableField("category_level")
    private Integer categoryLevel;

    @ApiModelProperty(value = "父层级id 父id 上一级依赖的id，1级分类则为0，二级三级分别依赖上一级")
    @TableField("parent_id")
    private Integer parentId;

    @ApiModelProperty(value = "图标 logo")
    @TableField("category_icon")
    private String categoryIcon;

    @ApiModelProperty(value = "口号")
    @TableField("category_slogan")
    private String categorySlogan;

    @ApiModelProperty(value = "分类图")
    @TableField("category_pic")
    private String categoryPic;

    @ApiModelProperty(value = "背景颜色")
    @TableField("category_bg_color")
    private String categoryBgColor;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    //实现首页的类别显示
    @TableField(exist = false)
    private List<CategoryVO> categories;
    //实现首页分类商品推荐
    @TableField(exist = false)
    private List<ProductVO> products;

}
