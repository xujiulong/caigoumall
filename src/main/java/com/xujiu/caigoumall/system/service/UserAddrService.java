package com.xujiu.caigoumall.system.service;

import com.xujiu.caigoumall.system.entity.UserAddr;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;

/**
* @author 许久龙
* @since 2022-02-21
*/
public interface UserAddrService extends IService<UserAddr> {
    /**
     * 查询地址列表通过用户id
     * @param userId
     * @return
     */
    ResultVO listAddrsByUid(Integer userId);

    /**
     * 创建新的收货地址
     * @param address 用户提交的收货地址数据
     */
    ResultVO addNewAddress(UserAddr address);

    /**
     * 删除收货地址
     * @param aid 收货地址id
     */
    ResultVO delete(Integer aid);


    /**
     * 根据收货地址数据的id，查询收货地址详情
     * @param aid 收货地址id
     * @return 匹配的收货地址详情
     */
    ResultVO getByAid(Integer aid);

    /**
     * 根据aid修改地址信息
     * @param address
     * @return
     */
    ResultVO updateByAid(UserAddr address);
}

