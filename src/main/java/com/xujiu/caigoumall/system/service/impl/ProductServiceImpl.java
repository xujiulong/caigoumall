package com.xujiu.caigoumall.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xujiu.caigoumall.system.entity.ProductImg;
import com.xujiu.caigoumall.system.entity.ProductParams;
import com.xujiu.caigoumall.system.entity.ProductSku;
import com.xujiu.caigoumall.system.entity.Users;
import com.xujiu.caigoumall.system.entity.vo.CategoryVO;
import com.xujiu.caigoumall.system.entity.vo.ProductCommentsVO;
import com.xujiu.caigoumall.system.entity.vo.ProductVO;
import com.xujiu.caigoumall.system.mapper.*;
import com.xujiu.caigoumall.system.service.CategoryService;
import com.xujiu.caigoumall.system.service.ProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;
import com.xujiu.caigoumall.util.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 商品 商品信息相关表：分类表，商品图片表，商品规格表，商品参数表 服务实现类
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, ProductVO> implements ProductService {
    @Autowired
    ProductMapper productMapper;
    @Autowired
    ProductImgMapper productImgMapper;
    @Autowired
    CategoryMapper categoryMapper;
    @Autowired
    ProductSkuMapper productSkuMapper;
    @Autowired
    ProductParamsMapper productParamsMapper;
    @Autowired
    CategoryService categoryService;


    @Override
    public ResultVO getListRecommends() {
        //查询创建时间最新并且状态正常的3个商品
        QueryWrapper<ProductVO> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("create_time").eq("product_status", 1).last("limit 0,3");
        List<ProductVO> productVOS = productMapper.selectList(wrapper);
        //根据商品id查询商品图⽚
        for (ProductVO productVO : productVOS) {
            List<ProductImg> productImgs = getImgbyProductId(productVO.getProductId());
            productVO.setImgs(productImgs);
        }
        if (productVOS.size() > 0) {
            return new ResultVO(200, "查询成功", productVOS);
        }
        return new ResultVO(401, "查询失败", null);
    }

    @Override
    public ResultVO getCategoryRecommends() {
        //查询所有的⼀级分类
        QueryWrapper<CategoryVO> wrapper = new QueryWrapper<>();
        wrapper.eq("category_level", 1);
        List<CategoryVO> categoryVOS = categoryMapper.selectList(wrapper);
        //查询每个分类下销量前6的商品
        for (CategoryVO categoryVO : categoryVOS) {
            QueryWrapper<ProductVO> wrapper1 = new QueryWrapper<>();
            wrapper1.eq("root_category_id", categoryVO.getCategoryId()).orderByDesc("sold_num").last("limit 0,6");
            List<ProductVO> productVOS = productMapper.selectList(wrapper1);
            categoryVO.setProducts(productVOS);
            //根据商品id查询商品图⽚
            for (ProductVO productVO : productVOS) {
                List<ProductImg> productImgs = getImgbyProductId(productVO.getProductId());
                productVO.setImgs(productImgs);
            }
        }
        if (categoryVOS.size() > 0) {
            return new ResultVO(200, "查询成功", categoryVOS);
        }
        return new ResultVO(401, "查询失败", null);
    }

    @Override
    public ResultVO getProductDetailInfo(String productId) {
        ProductVO product = getProductbyProductId(productId);
        List<ProductImg> img = getImgbyProductId(productId);
        List<ProductSku> sku = getSkubyProductId(productId);
        Map<String, Object> productMap = new HashMap<>();
        productMap.put("product",product);
        productMap.put("productImgs",img);
        productMap.put("productSkus",sku);
        if (productMap.size() > 0) {
            return new ResultVO(200, "查询成功", productMap);
        }
        return new ResultVO(401, "查询失败", null);
    }

    @Override
    public ResultVO getProductParamsById(String productId) {
        QueryWrapper<ProductParams> wrapper = new QueryWrapper<>();
        wrapper.eq("product_id", productId);
        List<ProductParams> productParams = productParamsMapper.selectList(wrapper);
        if (productParams.size() > 0) {
            return new ResultVO(200, "查询成功", productParams.get(0));
        }
        return new ResultVO(401, "查询失败", null);
    }


    @Override
    public List<ProductImg> getImgbyProductId(String productId) {
        QueryWrapper<ProductImg> wrapper = new QueryWrapper<>();
        wrapper.eq("product_id", productId);
        List<ProductImg> productImgs = productImgMapper.selectList(wrapper);
        return productImgs;
    }

    @Override
    public ProductImg getMainImgbyProductId(String productId) {
        QueryWrapper<ProductImg> wrapper = new QueryWrapper<>();
        wrapper.eq("product_id", productId).eq("is_main",1);
        List<ProductImg> productImgs = productImgMapper.selectList(wrapper);
        return productImgs.get(0);
    }

    @Override
    public List<ProductSku> getSkubyProductId(String productId) {
        QueryWrapper<ProductSku> wrapper = new QueryWrapper<>();
        wrapper.eq("product_id", productId).eq("status",1);
        List<ProductSku> productSku = productSkuMapper.selectList(wrapper);
        return productSku;
    }

    @Override
    public ProductVO getProductbyProductId(String productId) {
        QueryWrapper<ProductVO> wrapper = new QueryWrapper<>();
        wrapper.eq("product_id", productId).eq("product_status",1);
        List<ProductVO> productVOS = productMapper.selectList(wrapper);
        return productVOS.get(0);
    }

    @Override
    public ResultVO getBrandsByCategoryId(int categoryId) {
        QueryWrapper<ProductVO> wrapper = new QueryWrapper<>();
        wrapper.eq("category_id", categoryId).eq("product_status",1).select("product_id");
        List<ProductVO> productVOS = productMapper.selectList(wrapper);
        List<String> brands = new ArrayList<>();
        for (ProductVO productVO:productVOS){
            QueryWrapper<ProductParams> wrapper1 = new QueryWrapper<>();
            wrapper1.eq("product_id", productVO.getProductId()).select("DISTINCT brand");
            ProductParams productParams = productParamsMapper.selectOne(wrapper1);
            if(productParams!=null){
                brands.add(productParams.getBrand());
            }else {
                continue;
            }
        }
        //去重
        List<String> newList = new ArrayList<>(brands.size());
        brands.forEach(i -> {
            if (!newList.contains(i)) { // 如果新集合中不存在则插入
                newList.add(i);
            }
        });
        if (newList.size() > 0) {
            return new ResultVO(200, "查询成功",newList);
        }
        return new ResultVO(401, "查询失败", null);
    }

    @Override
    public ResultVO searchProduct(String keyword, int pageNum, int limit) {
        //当前页数据
        int start = (pageNum-1)*limit;
        QueryWrapper<ProductVO> wrapper = new QueryWrapper<>();
        wrapper.like("product_name", keyword).last("limit " + start + "," + limit);
        List<ProductVO> productVOS = productMapper.selectList(wrapper);
         for (ProductVO productVO : productVOS) {
            productVO.setImgs(getImgbyProductId(productVO.getProductId()));
            productVO.setSkus(getSkubyProductId(productVO.getProductId()));
        }

        //查询当前关键字查询的商品的总数
        QueryWrapper<ProductVO> wrapper1 = new QueryWrapper<>();
        wrapper1.like("product_name", keyword);
        int total = productMapper.selectCount(wrapper1);

        //计算总页数
        int pageCount = total%limit==0? total/limit : total/limit+1;
        if (productVOS.size() > 0) {
            return new ResultVO(200, "查询成功",  new PageHelper<ProductVO>(total,pageCount,productVOS));
        }
        return new ResultVO(401, "查询失败", null);
    }

    @Override
    public ResultVO getBrandsByKeyword(String kw) {
        QueryWrapper<ProductVO> wrapper = new QueryWrapper<>();
        wrapper.like("product_name", kw).eq("product_status",1).select("product_id");
        List<ProductVO> productVOS = productMapper.selectList(wrapper);
        List<String> brands = new ArrayList<>();
        for (ProductVO productVO:productVOS){
            QueryWrapper<ProductParams> wrapper1 = new QueryWrapper<>();
            wrapper1.eq("product_id", productVO.getProductId()).select("DISTINCT brand");
            ProductParams productParams = productParamsMapper.selectOne(wrapper1);
            if(productParams!=null){
                brands.add(productParams.getBrand());
            }else {
                continue;
            }
        }
        //去重
        List<String> newList = new ArrayList<>(brands.size());
        brands.forEach(i -> {
            if (!newList.contains(i)) { // 如果新集合中不存在则插入
                newList.add(i);
            }
        });
        if (newList.size() > 0) {
            return new ResultVO(200, "查询成功",newList);
        }
        return new ResultVO(401, "查询失败", null);
    }

    @Override
    public ResultVO getProductsByCategoryId(int cid, int pageNum, int limit) {
        //当前页数据
        int start = (pageNum-1)*limit;
        QueryWrapper<ProductVO> wrapper = new QueryWrapper<>();
        wrapper.eq("category_id", cid).eq("product_status",1).last("limit " + start + "," + limit);
        List<ProductVO> productVOS = productMapper.selectList(wrapper);
        for (ProductVO productVO : productVOS) {
            productVO.setImgs(getImgbyProductId(productVO.getProductId()));
            productVO.setSkus(getSkubyProductId(productVO.getProductId()));
        }

        //查询当前类别id查询的商品的总数
        QueryWrapper<ProductVO> wrapper1 = new QueryWrapper<>();
        wrapper1.eq("category_id", cid).eq("product_status",1);
        int total = productMapper.selectCount(wrapper1);
        System.out.println("wwww"+total);
        //计算总页数
        int pageCount = total%limit==0? total/limit : total/limit+1;
        if (productVOS.size() > 0) {
            return new ResultVO(200, "查询成功",  new PageHelper<ProductVO>(total,pageCount,productVOS));
        }
        return new ResultVO(401, "查询失败", null);
    }


}


