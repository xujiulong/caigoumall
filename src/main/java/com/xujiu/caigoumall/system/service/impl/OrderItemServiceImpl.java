package com.xujiu.caigoumall.system.service.impl;

import com.xujiu.caigoumall.system.entity.OrderItem;
import com.xujiu.caigoumall.system.mapper.OrderItemMapper;
import com.xujiu.caigoumall.system.service.OrderItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 订单项/快照  服务实现类
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Service
public class OrderItemServiceImpl extends ServiceImpl<OrderItemMapper, OrderItem> implements OrderItemService {


}


