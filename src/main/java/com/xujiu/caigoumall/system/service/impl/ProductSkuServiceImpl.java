package com.xujiu.caigoumall.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xujiu.caigoumall.system.entity.ProductSku;
import com.xujiu.caigoumall.system.mapper.ProductSkuMapper;
import com.xujiu.caigoumall.system.service.ProductSkuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品规格 每一件商品都有不同的规格，不同的规格又有不同的价格和优惠力度，规格表为此设计 服务实现类
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Service
public class ProductSkuServiceImpl extends ServiceImpl<ProductSkuMapper, ProductSku> implements ProductSkuService {
    @Autowired
    ProductSkuMapper productSkuMapper;

    @Override
    public ProductSku selectBySkuId(String skuId) {
        QueryWrapper<ProductSku> wrapper = new QueryWrapper<>();
        wrapper.eq("sku_id", skuId).eq("status", 1);
        ProductSku productSku = productSkuMapper.selectOne(wrapper);
        return productSku;
    }
}


