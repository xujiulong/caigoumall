package com.xujiu.caigoumall.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xujiu.caigoumall.system.entity.DictDistrict;
import com.xujiu.caigoumall.system.mapper.DictDistrictMapper;
import com.xujiu.caigoumall.system.service.DictDistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 地区字典 服务实现类
 *
 * @author 许久龙
 * @since 2022-02-27
 */
@Service
public class DictDistrictServiceImpl extends ServiceImpl<DictDistrictMapper, DictDistrict> implements DictDistrictService {
    @Autowired
    private DictDistrictMapper dictDistrictMapper;

    @Override
    public List<DictDistrict> getByParent(String parent) {
        QueryWrapper<DictDistrict> wrapper = new QueryWrapper<>();
        wrapper.eq("parent", parent).orderByAsc("code");
        List<DictDistrict> dictDistricts = dictDistrictMapper.selectList(wrapper);
        for (DictDistrict district : dictDistricts) {
            district.setId(null);
            district.setParent(null);
        }
        return dictDistricts;
    }

    @Override
    public String getNameByCode(String code) {
        QueryWrapper<DictDistrict> wrapper = new QueryWrapper<>();
        wrapper.eq("code", code).select("name");
        DictDistrict dictDistrict = dictDistrictMapper.selectOne(wrapper);
        return dictDistrict.getName();
    }
}
