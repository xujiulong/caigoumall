package com.xujiu.caigoumall.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xujiu.caigoumall.system.entity.vo.CategoryVO;

import java.util.List;

/**
* @author 许久龙
* @since 2022-02-21
*/
public interface CategoryService extends IService<CategoryVO> {
    /**
     * 查询当前指定分类级别下的所有子列表
     * @return
     */
     List<CategoryVO> listCategories(int categoryLevel);
}

