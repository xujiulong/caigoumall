package com.xujiu.caigoumall.system.service;

import com.xujiu.caigoumall.system.entity.IndexImg;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;

/**
 * @author 许久龙
 * @since 2022-02-21
 */
public interface IndexImgService extends IService<IndexImg> {
    /**
     * 查询轮播图信息
     * @return
     */
    public ResultVO getIndexImg();
}

