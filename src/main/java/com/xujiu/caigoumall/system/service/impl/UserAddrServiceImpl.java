package com.xujiu.caigoumall.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xujiu.caigoumall.system.entity.UserAddr;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;
import com.xujiu.caigoumall.system.mapper.UserAddrMapper;
import com.xujiu.caigoumall.system.service.DictDistrictService;
import com.xujiu.caigoumall.system.service.UserAddrService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 用户地址  服务实现类
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Service
public class UserAddrServiceImpl extends ServiceImpl<UserAddrMapper, UserAddr> implements UserAddrService {

    @Autowired
    UserAddrMapper userAddrMapper;
    @Autowired
    DictDistrictService districtService;

    @Override
    public ResultVO listAddrsByUid(Integer userId) {
        QueryWrapper<UserAddr> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        wrapper.eq("status", 1);
        List<UserAddr> userAddrs = userAddrMapper.selectList(wrapper);
        if (userAddrs.size() > 0) {
            return new ResultVO(200, "查询成功", userAddrs);
        }
        return new ResultVO(401, "查询失败", null);
    }

    @Override
    @Transactional
    public ResultVO addNewAddress(UserAddr address) {

        // 补全数据：省、市、区的名称
        String provinceName = districtService.getNameByCode(address.getProvinceCode());
        String cityName = districtService.getNameByCode(address.getCityCode());
        String areaName = districtService.getNameByCode(address.getAreaCode());
        address.setProvinceName(provinceName);
        address.setCityName(cityName);
        address.setAreaName(areaName);

        // 补全数据：根据以上统计的数量，得到正确的isDefault值(是否默认：0-不默认，1-默认)，并封装
        // Integer isDefault = count == 0 ? 1 : 0;
        address.setCommonAddr(0);
        address.setStatus(1);

        // 调用addressMapper的insert(Address address)方法插入收货地址数据，并获取返回的受影响行数
        int insert = userAddrMapper.insert(address);
        // 判断受影响行数是否不为1
        if (insert > 0) {
            return new ResultVO(200, "添加成功！", address);
        } else {
            return new ResultVO(401, "添加失败！", null);
        }
    }

    @Override
    @Transactional
    public ResultVO delete(Integer aid) {

        // 根据参数aid，调用deleteByAid()执行删除

        Integer rows = userAddrMapper.deleteById(aid);
        if (rows > 0) {
            return new ResultVO(200, "删除成功！", null);
        } else {
            return new ResultVO(401, "删除失败！", null);
        }
    }

    @Override
    public ResultVO getByAid(Integer aid) {
        // 根据收货地址数据id，查询收货地址详情
        UserAddr address = userAddrMapper.selectById(aid);

        if (address == null) {
            return new ResultVO(401, "查询失败！", null);
        } else {
            return new ResultVO(200, "查询成功！", address);
        }
    }

    @Override
    public ResultVO updateByAid(UserAddr address) {
        // 补全数据：省、市、区的名称
        String provinceName = districtService.getNameByCode(address.getProvinceCode());
        String cityName = districtService.getNameByCode(address.getCityCode());
        String areaName = districtService.getNameByCode(address.getAreaCode());
        address.setProvinceName(provinceName);
        address.setCityName(cityName);
        address.setAreaName(areaName);

//        // 补全数据：根据以上统计的数量，得到正确的isDefault值(是否默认：0-不默认，1-默认)，并封装
//        Integer isDefault = count == 0 ? 1 : 0;
        address.setCommonAddr(0);
        address.setStatus(1);
        // 补全数据：2项日志

        // 调用addressMapper的insert(Address address)方法插入收货地址数据，并获取返回的受影响行数
        Integer rows = userAddrMapper.updateById(address);
        // 判断受影响行数是否不为1
        if (rows > 0) {
            return new ResultVO(200, "修改成功！", address);
        } else {
            return new ResultVO(401, "修改失败！", null);
        }
    }
}


