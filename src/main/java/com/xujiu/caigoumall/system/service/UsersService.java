package com.xujiu.caigoumall.system.service;

import com.xujiu.caigoumall.system.entity.Users;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;

/**
* @author 许久龙
* @since 2022-02-21
*/
public interface UsersService extends IService<Users> {

    /**
     * 用户注册
     * @param users
     * @return
     */
     ResultVO regist(Users users);

    /**
     * 用户登录
     * @param users
     * @return
     */
     ResultVO login(Users users);
}

