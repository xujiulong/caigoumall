package com.xujiu.caigoumall.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.xujiu.caigoumall.system.entity.ProductImg;
import com.xujiu.caigoumall.system.entity.ProductSku;
import com.xujiu.caigoumall.system.entity.ShoppingCart;
import com.xujiu.caigoumall.system.entity.vo.ProductVO;
import com.xujiu.caigoumall.system.entity.vo.ShoppingCartVO;
import com.xujiu.caigoumall.system.mapper.ShoppingCartMapper;
import com.xujiu.caigoumall.system.service.ProductService;
import com.xujiu.caigoumall.system.service.ProductSkuService;
import com.xujiu.caigoumall.system.service.ShoppingCartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 购物车  服务实现类
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCartVO> implements ShoppingCartService {
    @Autowired
    ShoppingCartMapper shoppingCartMapper;

    @Autowired
    ProductService productService;
    @Autowired
    ProductSkuService productSkuService;

    @Override
    public ResultVO listShoppingCartsByUserId(Integer userId) {
        QueryWrapper<ShoppingCartVO> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        List<ShoppingCartVO> shoppingCarts = shoppingCartMapper.selectList(wrapper);
        List<ShoppingCartVO> shoppingCartVOS = getVO(shoppingCarts);
            return new ResultVO(200, "查询成功", shoppingCartVOS);
    }

    @Override
    public ResultVO listShoppingCartsByCids(String cartIds) {
        //将根据","分隔的cartIds转储在list中
        String[] arr = cartIds.split(",");
        List<String> list = new ArrayList();
        for (int i = 0; i < arr.length; i++) {
            list.add(arr[i]);
        }
        List<ShoppingCartVO> shoppingCarts = shoppingCartMapper.selectBatchIds(list);
        List<ShoppingCartVO> shoppingCartVOS = getVO(shoppingCarts);
        if (shoppingCartVOS.size() > 0) {
            return new ResultVO(200, "查询成功", shoppingCartVOS);
        }
        return new ResultVO(401, "查询失败", null);
    }

    /**
     * 完善ShoppingCartVO的字段
     *
     * @param shoppingCartVOS
     * @return
     */
    private List<ShoppingCartVO> getVO(List<ShoppingCartVO> shoppingCartVOS) {
        for (ShoppingCartVO shoppingCartVO : shoppingCartVOS) {
            //查询商品名称
            ProductVO product = productService.getProductbyProductId(shoppingCartVO.getProductId());
            shoppingCartVO.setProductName(product.getProductName());
            //查询商品主照片
            ProductImg mainImg = productService.getMainImgbyProductId(shoppingCartVO.getProductId());
            shoppingCartVO.setProductImg(mainImg.getUrl());
            //查询商品套餐原价 现价 名称 库存
            ProductSku productSku = productSkuService.selectBySkuId(shoppingCartVO.getSkuId());
            shoppingCartVO.setOriginalPrice(productSku.getOriginalPrice());
            shoppingCartVO.setSellPrice(productSku.getSellPrice());
            shoppingCartVO.setSkuName(productSku.getSkuName());
            shoppingCartVO.setSkuStock(productSku.getStock());
        }
        return shoppingCartVOS;
    }

    @Override
    @Transactional
    public ResultVO addShoppingCart(ShoppingCart shoppingCart) {
        Map<String, Object> map = BeanUtil.beanToMap(shoppingCart);
        ShoppingCartVO shoppingCartVO = BeanUtil.mapToBean(map, ShoppingCartVO.class, false);
        int insert = shoppingCartMapper.insert(shoppingCartVO);
        if (insert > 0) {
            return new ResultVO(200, "添加成功", null);
        }
        return new ResultVO(401, "查询失败", null);
    }

    @Override
    @Transactional
    public ResultVO updateCartNum(Integer cartId, Integer cartNum) {
        UpdateWrapper<ShoppingCartVO> wrapper = new UpdateWrapper<>();
        wrapper.set("cart_num",cartNum).eq("cart_id",cartId);
        int i = shoppingCartMapper.update(null,wrapper);
        if (i > 0) {
            return new ResultVO(200, "修改成功", null);
        }
        return new ResultVO(401, "修改失败", null);
    }

    @Override
    @Transactional
    public ResultVO removeByCids(String cartIds) {
        //将根据","分隔的cartIds转储在list中
        String[] arr = cartIds.split(",");
        List<String> list = new ArrayList();
        for (int i = 0; i < arr.length; i++) {
            list.add(arr[i]);
        }
        int i = shoppingCartMapper.deleteBatchIds(list);
        if (i > 0) {
            return new ResultVO(200, "删除成功", null);
        }
        return new ResultVO(401, "删除失败", null);
    }
}


