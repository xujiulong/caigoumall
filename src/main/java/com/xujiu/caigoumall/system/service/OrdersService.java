package com.xujiu.caigoumall.system.service;

import com.xujiu.caigoumall.system.entity.Orders;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xujiu.caigoumall.system.entity.vo.OrdersVO;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Map;

/**
 * @author 许久龙
 * @since 2022-02-21
 */
public interface OrdersService extends IService<OrdersVO> {

    /**
     * 向微信支付端响应订单信息
     * @param cids  多个购物车id
     * @param order 订单
     * @return
     */
    ResultVO sendToVXPay(String cids, Orders order);

    /**
     * 更新订单状态
     * @param orderId
     * @param status
     * @return
     */
    int updateOrderStatus(String orderId, String status);

    /**
     * 关闭订单 还原库存
     * @param orderId
     */
    void closeOrder(String orderId);

    /**
     * 查询订单状态根据orderId
     * @param orderId
     * @return
     */
    ResultVO getOrderStatusById(String orderId);

    /**
     * 查询全部订单分页
     * @param userId
     * @param status
     * @param pageNum
     * @param limit
     * @return
     */
    ResultVO listOrders(String userId, String status, int pageNum, int limit);

    /**
     * 添加订单（生成商品快照 加销量 减库存 删购物车）
     * @param cids  多个购物车id
     * @param order 订单
     * @return
     */
    Map<String,String> addOrder(String cids, Orders order);




}

