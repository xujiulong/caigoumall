package com.xujiu.caigoumall.system.service.impl;

import com.xujiu.caigoumall.system.entity.ProductImg;
import com.xujiu.caigoumall.system.mapper.ProductImgMapper;
import com.xujiu.caigoumall.system.service.ProductImgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 商品图片  服务实现类
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Service
public class ProductImgServiceImpl extends ServiceImpl<ProductImgMapper, ProductImg> implements ProductImgService {


}


