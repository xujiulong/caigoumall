package com.xujiu.caigoumall.system.service;

import com.xujiu.caigoumall.system.entity.ShoppingCart;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xujiu.caigoumall.system.entity.vo.ShoppingCartVO;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;

/**
* @author 许久龙
* @since 2022-02-21
*/
public interface ShoppingCartService extends IService<ShoppingCartVO> {
  /**
   * 根据userId查询用户购物车信息（商品名称 图片）
   * @param userId
   * @return
   */
    ResultVO listShoppingCartsByUserId(Integer userId);

  /**
   * 根据cartIds查询购物车信息（多个购物车id用，分隔）
   * @param cartIds
   * @return
   */
    ResultVO listShoppingCartsByCids(String cartIds);

  /**
   * 根据用户id添加购物车
   * @param shoppingCart
   * @return
   */
    ResultVO addShoppingCart(ShoppingCart shoppingCart);

  /**
   * 修改购物车数量
   * @param cartId
   * @param cartNum
   * @return
   */
    ResultVO updateCartNum(Integer cartId,Integer cartNum);

  /**
   * 根据cartIds删除购物车（多个购物车id用，分隔）
   * @param cartIds
   * @return
   */
    ResultVO removeByCids(String cartIds);
}

