package com.xujiu.caigoumall.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xujiu.caigoumall.system.entity.vo.ProductCommentsVO;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;

/**
* @author 许久龙
* @since 2022-02-21
*/
public interface ProductCommentsService extends IService<ProductCommentsVO> {
    /**
     * 根据商品id查询评论统计
     * @param productId
     * @return
     */
    ResultVO getCommentsCountByProductId(String productId);

    /**
     *  根据商品id查询评论信息分页
     * @param productId 商品id
     * @param pageNum 当前页码
     * @param limit 每页显示条数
     * @return
     */
    ResultVO listCommontsByProductId(String productId,int pageNum,int limit);
}

