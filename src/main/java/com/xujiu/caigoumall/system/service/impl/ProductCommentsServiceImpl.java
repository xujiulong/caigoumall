package com.xujiu.caigoumall.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xujiu.caigoumall.system.entity.Users;
import com.xujiu.caigoumall.system.entity.vo.ProductCommentsVO;
import com.xujiu.caigoumall.system.mapper.ProductCommentsMapper;
import com.xujiu.caigoumall.system.mapper.UsersMapper;
import com.xujiu.caigoumall.system.service.ProductCommentsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xujiu.caigoumall.util.PageHelper;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * 商品评价  服务实现类
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Service
public class ProductCommentsServiceImpl extends ServiceImpl<ProductCommentsMapper, ProductCommentsVO> implements ProductCommentsService {
    @Autowired
    ProductCommentsMapper productCommentsMapper;
    @Autowired
    UsersMapper usersMapper;

    @Override
    public ResultVO getCommentsCountByProductId(String productId) {

        //1.查询当前商品评价的总数
        QueryWrapper<ProductCommentsVO> wrapper = new QueryWrapper<>();
        wrapper.eq("product_id", productId);
        int total = productCommentsMapper.selectCount(wrapper);

        //2.查询好评评价数
        wrapper.eq("comm_type", 1);
        int goodTotal = productCommentsMapper.selectCount(wrapper);

        //3.查询中评评价数
        QueryWrapper<ProductCommentsVO> wrapper1 = new QueryWrapper<>();
        wrapper1.eq("product_id", productId).eq("comm_type", 0);
        int midTotal = productCommentsMapper.selectCount(wrapper1);

        //4.查询差评评价数
        QueryWrapper<ProductCommentsVO> wrapper2 = new QueryWrapper<>();
        wrapper2.eq("product_id", productId).eq("comm_type", -1);
        int badTotal = productCommentsMapper.selectCount(wrapper2);

        //5.计算好评率
        double percent = (Double.parseDouble(goodTotal + "") / Double.parseDouble(total + "")) * 100;
        String percentValue = (percent + "").substring(0, (percent + "").lastIndexOf(".") + 3);

        HashMap<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("goodTotal", goodTotal);
        map.put("midTotal", midTotal);
        map.put("badTotal", badTotal);
        map.put("percent", percentValue);
        if (map.size() > 0) {
            return new ResultVO(200, "查询成功", map);
        }
        return new ResultVO(401, "查询失败", null);
    }

    @Override
    public ResultVO listCommontsByProductId(String productId, int pageNum, int limit) {
        //当前页数据
        int start = (pageNum - 1) * limit;
        QueryWrapper<ProductCommentsVO> wrapper = new QueryWrapper<>();
        wrapper.eq("product_id", productId).last("limit " + start + "," + limit);
        List<ProductCommentsVO> productCommentsVOS = productCommentsMapper.selectList(wrapper);
        for (ProductCommentsVO productCommentsVO : productCommentsVOS) {
            QueryWrapper<Users> wrapper1 = new QueryWrapper<>();
            wrapper1.eq("user_id", productCommentsVO.getUserId()).select("username", "nickname", "user_img");
            Users users = usersMapper.selectOne(wrapper1);
            productCommentsVO.setUsername(users.getUsername());
            productCommentsVO.setNickname(users.getNickname());
            productCommentsVO.setUserImg(users.getUserImg());
        }

        //查询当前商品评价的总数
        QueryWrapper<ProductCommentsVO> wrapper1 = new QueryWrapper<>();
        wrapper1.eq("product_id", productId);
        int total = productCommentsMapper.selectCount(wrapper1);

        //计算总页数
        int pageCount = total % limit == 0 ? total / limit : total / limit + 1;
        if (productCommentsVOS.size() > 0) {
            return new ResultVO(200, "查询成功", new PageHelper<ProductCommentsVO>(total, pageCount, productCommentsVOS));
        }
        return new ResultVO(401, "查询失败", null);
    }

}


