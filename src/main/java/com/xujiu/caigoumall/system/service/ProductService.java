package com.xujiu.caigoumall.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xujiu.caigoumall.system.entity.ProductImg;
import com.xujiu.caigoumall.system.entity.ProductSku;
import com.xujiu.caigoumall.system.entity.vo.ProductVO;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 许久龙
 * @since 2022-02-21
 */
public interface ProductService extends IService<ProductVO> {
    /**
     * 查询新品推荐最新上市的3款
     *
     * @return
     */
    ResultVO getListRecommends();

    /**
     * 按照商品的⼀级分类查询销量最⾼的6个商品
     *
     * @return
     */
    ResultVO getCategoryRecommends();

    /**
     * 根据商品id查询当前商品的详细信息(基本信息 套餐 图片）
     *
     * @return
     */
    ResultVO getProductDetailInfo(String productId);

    /**
     * 根据商品id查询当前商品的参数信息
     *
     * @return
     */
    ResultVO getProductParamsById(String productId);

    /**
     * 根据商品id查询当前商品的图⽚
     *
     * @return
     */
    List<ProductImg> getImgbyProductId(String productId);

    /**
     * 根据商品id查询当前商品的主图⽚
     * @param productId
     * @return
     */
     ProductImg getMainImgbyProductId(String productId);
    /**
     * 根据商品id查询当前商品的套餐
     *
     * @return
     */
    List<ProductSku> getSkubyProductId(String productId);

    /**
     * 根据商品id查询当前商品的基本信息（名称，销量，内容）
     *
     * @return
     */
    ProductVO getProductbyProductId(String productId);

    /**
     *根据类别id查询此类别下的商品品牌列表
     * @param categoryId
     * @return
     */
    ResultVO getBrandsByCategoryId(int categoryId);

    /**
     * 根据关键字模糊搜索商品信息
     * @param keyword
     * @param start
     * @param limit
     * @return
     */
    ResultVO searchProduct(@Param("kw") String keyword,
                           @Param("start") int start,
                           @Param("limit") int limit);

    /**
     * 根据搜索关键字查询相关商品的品牌列表
     * @param kw
     * @return
     */
    ResultVO getBrandsByKeyword(String kw);

    /**
     * 根据类别id查商品信息
     * @param cid
     * @param pageNum
     * @param limit
     * @return
     */
    ResultVO getProductsByCategoryId(int cid,int pageNum,int limit);
}

