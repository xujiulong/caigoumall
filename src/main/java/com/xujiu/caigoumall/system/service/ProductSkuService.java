package com.xujiu.caigoumall.system.service;

import com.xujiu.caigoumall.system.entity.ProductSku;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 许久龙
* @since 2022-02-21
*/
public interface ProductSkuService extends IService<ProductSku> {
    /**
     * 根据skuId查询套餐信息
     * @param skuId
     * @return
     */
    ProductSku selectBySkuId(String skuId );
}

