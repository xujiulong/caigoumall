package com.xujiu.caigoumall.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xujiu.caigoumall.system.entity.IndexImg;
import com.xujiu.caigoumall.system.mapper.IndexImgMapper;
import com.xujiu.caigoumall.system.service.IndexImgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 轮播图  服务实现类
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Service
public class IndexImgServiceImpl extends ServiceImpl<IndexImgMapper, IndexImg> implements IndexImgService {
    @Autowired
    IndexImgMapper indexImgMapper;

    public ResultVO getIndexImg() {
        QueryWrapper<IndexImg> wrapper = new QueryWrapper<>();
        wrapper.eq("status", 1);
        wrapper.orderByAsc("seq");
        List<IndexImg> indexImgs = indexImgMapper.selectList(wrapper);
        if (indexImgs.size() > 0) {
            return new ResultVO(200, "查询成功", indexImgs);
        }
        return new ResultVO(401, "查询失败", null);
    }
}


