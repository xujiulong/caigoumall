package com.xujiu.caigoumall.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.TypeReference;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.github.wxpay.sdk.WXPay;
import com.xujiu.caigoumall.config.MyPayConfig;
import com.xujiu.caigoumall.system.entity.OrderItem;
import com.xujiu.caigoumall.system.entity.Orders;
import com.xujiu.caigoumall.system.entity.ProductSku;
import com.xujiu.caigoumall.system.entity.Users;
import com.xujiu.caigoumall.system.entity.vo.*;
import com.xujiu.caigoumall.system.mapper.OrderItemMapper;
import com.xujiu.caigoumall.system.mapper.OrdersMapper;
import com.xujiu.caigoumall.system.mapper.ProductMapper;
import com.xujiu.caigoumall.system.mapper.ProductSkuMapper;
import com.xujiu.caigoumall.system.service.OrdersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xujiu.caigoumall.system.service.ShoppingCartService;
import com.xujiu.caigoumall.util.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.*;

/**
 * 订单  服务实现类
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, OrdersVO> implements OrdersService {
    @Autowired
    ShoppingCartService shoppingCartService;
    @Autowired
    OrdersMapper ordersMapper;
    @Autowired
    OrderItemMapper orderItemMapper;
    @Autowired
    ProductMapper productMapper;
    @Autowired
    ProductSkuMapper productSkuMapper;

    @Override
    @Transactional
    public ResultVO sendToVXPay(String cids, Orders order) {
        synchronized (this) {
            try {
                Map<String, String> orderInfo = addOrder(cids, order);
                if (orderInfo != null) {
                    String orderId = orderInfo.get("orderId");
                    //设置当前订单信息
                    HashMap<String, String> data = new HashMap<>();
                    data.put("body", orderInfo.get("productNames"));                  //商品描述
                    data.put("out_trade_no", orderId);                                //使用当前用户订单的编号作为当前支付交易的交易号
                    data.put("fee_type", "CNY");                                      //支付币种
                    //data.put("total_fee",order.getActualAmount()*100+"");           //支付金额
                    data.put("total_fee", "1");
                    data.put("trade_type", "NATIVE");                                  //交易类型
                    data.put("notify_url", "http://hi7ey8.natappfree.cc/pay/callback");//设置支付完成时的回调方法接口
//                     data.put("notify_url", "http://101.201.78.85:8084/pay/callback");

                    //发送请求，获取响应
                    //微信支付：申请支付连接
                    WXPay wxPay = new WXPay(new MyPayConfig());
                    Map<String, String> resp = null;
                    resp = wxPay.unifiedOrder(data);
                    orderInfo.put("payUrl", resp.get("code_url"));
                    //orderInfo中包含：订单编号，购买的商品名称，支付链接
                    return new ResultVO(200, "提交订单成功！", orderInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return new ResultVO(401, "提交订单失败！", null);
        }
    }

    @Override
    @Transactional
    public int updateOrderStatus(String orderId, String status) {
        UpdateWrapper<OrdersVO> wrapper = new UpdateWrapper<>();
        wrapper.set("status", status).eq("order_id", orderId);
        int update = ordersMapper.update(null, wrapper);
        return update;
    }

    @Override
    @Transactional
    public void closeOrder(String orderId) {
        synchronized (this) {
            //1.修改当前订单信息：status=6 已关闭  close_type=1 超时未支付
            OrdersVO cancleOrder = new OrdersVO();
            cancleOrder.setOrderId(orderId);
            cancleOrder.setStatus("6");  //已关闭
            cancleOrder.setCloseType(1); //关闭类型：超时未支付
            ordersMapper.updateById(cancleOrder);

            //2.查询全部订单快照
            QueryWrapper<OrderItem> wrapper = new QueryWrapper<>();
            wrapper.eq("order_id", orderId);
            List<OrderItem> orderItems = orderItemMapper.selectList(wrapper);
            for (OrderItem orderItem : orderItems) {
                //查询对应商品套餐信息
                ProductSku productSku = productSkuMapper.selectById(orderItem.getSkuId());
                //还原库存
                productSku.setStock(productSku.getStock() + orderItem.getBuyCounts());
                productSkuMapper.updateById(productSku);
            }
        }
    }

    @Override
    public ResultVO getOrderStatusById(String orderId) {
        OrdersVO ordersVO = ordersMapper.selectById(orderId);
        if (ordersVO != null) {
            return new ResultVO(200, "查询成功", ordersVO.getStatus());
        }
        return new ResultVO(401, "查询失败", null);
    }

    @Override
    public ResultVO listOrders(String userId, String status, int pageNum, int limit) {
        //当前页数据
        int start = (pageNum - 1) * limit;
        QueryWrapper<OrdersVO> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId).eq(status!=null,"status", status).last("limit " + start + "," + limit).orderByDesc("create_time");
        List<OrdersVO> ordersVOS = ordersMapper.selectList(wrapper);
        for (OrdersVO ordersVO : ordersVOS) {
            QueryWrapper<OrderItem> wrapper1 = new QueryWrapper<>();
            wrapper1.eq("order_id", ordersVO.getOrderId()).orderByDesc("create_time");
            List<OrderItem> orderItems = orderItemMapper.selectList(wrapper1);
            ordersVO.setOrderItems(orderItems);
        }

        //查询当前用户该状态的订单的总数
        QueryWrapper<OrdersVO> wrapper1 = new QueryWrapper<>();
        wrapper.eq("user_id", userId).eq("status", status);
        int total = ordersMapper.selectCount(wrapper1);

        //计算总页数
        int pageCount = total % limit == 0 ? total / limit : total / limit + 1;
        if (ordersVOS.size() > 0) {
            return new ResultVO(200, "查询成功", new PageHelper<OrdersVO>(total, pageCount, ordersVOS));
        }
        return new ResultVO(401, "查询失败", null);
    }


    @Override
    @Transactional
    public Map<String, String> addOrder(String cids, Orders order) {
        synchronized (this) {
            List<ShoppingCartVO> list = null;
            //1.查询购物车列表
            ResultVO resultVO = shoppingCartService.listShoppingCartsByCids(cids);
            if (resultVO.getCode() == 200) {
                Object data = resultVO.getData();
                list = Convert.convert(new TypeReference<List<ShoppingCartVO>>() {
                }, data);

                //2.校验库存是否充足
                boolean istrue = true;
                //产品名称（多个产品用,隔开）
                String untitled = "";
                for (ShoppingCartVO sc : list) {
                    if (Integer.parseInt(sc.getCartNum()) > sc.getSkuStock()) {
                        istrue = false;
                    }
                    //获取所有商品名称，以,分割拼接成字符串
                    untitled += sc.getProductName() + ",";
                }
                //去掉untitled末尾的","
                untitled.substring(0, untitled.length() - 1);
                if (istrue) {
                    //3.添加订单
                    //补全订单信息
                    order.setUntitled(untitled);
                    //订单状态 1:待付款 2:待发货 3:待收货 4:待评价 5:已完成 6:已关闭
                    order.setStatus("1");
                    //生成订单编号
                    String orderId = IdUtil.simpleUUID();
                    order.setOrderId(orderId);
                    Map<String, Object> map = BeanUtil.beanToMap(order);
                    OrdersVO ordersVO = BeanUtil.mapToBean(map, OrdersVO.class, false);
                    ordersMapper.insert(ordersVO);

                    for (ShoppingCartVO sc : list) {
                        //4.生成商品快照
                        int cnum = Integer.parseInt(sc.getCartNum());
                        //生成18位随机商品快照id
                        String itemId = RandomUtil.randomNumbers(18);
                        double totalPrice = sc.getSellPrice() * cnum;
//                    OrderItem orderItem = new OrderItem(orderId, sc.getProductId(), sc.getProductName(), sc.getProductImg(), sc.getSkuId(), sc.getSkuName(), new BigDecimal(sc.getSellPrice()), cnum, new BigDecimal(totalPrice), sc.getCreateTime(), 0);
                        OrderItem orderItem = new OrderItem();
                        orderItem.setItemId(itemId);
                        orderItem.setOrderId(orderId);
                        orderItem.setProductId(sc.getProductId());
                        orderItem.setProductName(sc.getProductName());
                        orderItem.setProductImg(sc.getProductImg());
                        orderItem.setSkuId(sc.getSkuId());
                        orderItem.setSkuName(sc.getSkuName());
                        orderItem.setProductPrice(BigDecimal.valueOf(sc.getSellPrice()));
                        orderItem.setBuyCounts(cnum);
                        orderItem.setTotalAmount(BigDecimal.valueOf(totalPrice));
                        orderItem.setBasketDate(sc.getCreateTime());
                        orderItem.setIsComment(0);


                        orderItemMapper.insert(orderItem);
                        //5.增加商品销量
                        ProductVO productVO = productMapper.selectById(sc.getProductId());
                        productVO.setSoldNum(productVO.getSoldNum() + Integer.parseInt(sc.getCartNum()));
                        productMapper.updateById(productVO);
                        //6.减少商品库存
                        ProductSku productSku = productSkuMapper.selectById(sc.getSkuId());
                        productSku.setStock(sc.getSkuStock() - Integer.parseInt(sc.getCartNum()));
                        productSkuMapper.updateById(productSku);
                    }
                    //7.删购物车
                    ResultVO resultVO1 = shoppingCartService.removeByCids(cids);
                    if (resultVO.getCode() == 200) {
                        //返回map信息 响应给微信端
                        HashMap<String, String> map1 = new HashMap<>();
                        map1.put("orderId", orderId);
                        map1.put("productNames", untitled);
                        return map1;
                    }
                }
            }
            return null;
        }
    }
}


