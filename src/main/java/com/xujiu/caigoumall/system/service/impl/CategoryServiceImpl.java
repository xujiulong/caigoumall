package com.xujiu.caigoumall.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xujiu.caigoumall.system.entity.vo.CategoryVO;
import com.xujiu.caigoumall.system.mapper.CategoryMapper;
import com.xujiu.caigoumall.system.service.CategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品分类 服务实现类
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, CategoryVO> implements CategoryService {

    @Autowired
    CategoryMapper categoryMapper;

    public List<CategoryVO> listCategories(int categoryLevel) {
        QueryWrapper<CategoryVO> wrapper = new QueryWrapper<>();
        wrapper.eq("category_level", categoryLevel);
        List<CategoryVO> categoryVOS = categoryMapper.selectList(wrapper);
        for (CategoryVO categorie : categoryVOS) {
            QueryWrapper<CategoryVO> wrapper1 = new QueryWrapper<>();
            wrapper1.eq("parent_id", categorie.getCategoryId());
            List<CategoryVO> categoryVOS1 = categoryMapper.selectList(wrapper1);
            categorie.setCategories(categoryVOS1);
            for (CategoryVO categorie1 : categoryVOS1) {
                QueryWrapper<CategoryVO> wrapper2= new QueryWrapper<>();
                wrapper2.eq("parent_id", categorie1.getCategoryId());
                List<CategoryVO> categoryVOS2 = categoryMapper.selectList(wrapper2);
                categorie1.setCategories(categoryVOS2);
            }
        }
        return categoryVOS;

//    public  CategoryVO selectParent(int parentId){
//        QueryWrapper<CategoryVO> Wrapper = new QueryWrapper<>();
//        Wrapper.eq("status",1);
//        Wrapper.orderByAsc("seq");
//        List<IndexImg> indexImgs = indexImgMapper.selectList(Wrapper);
//
//
//    return null;
}
    }


