package com.xujiu.caigoumall.system.service.impl;

import com.xujiu.caigoumall.system.entity.ProductParams;
import com.xujiu.caigoumall.system.mapper.ProductParamsMapper;
import com.xujiu.caigoumall.system.service.ProductParamsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 商品参数  服务实现类
 *
 * @author 许久龙
 * @since 2022-02-21
 */
@Service
public class ProductParamsServiceImpl extends ServiceImpl<ProductParamsMapper, ProductParams> implements ProductParamsService {


}


