package com.xujiu.caigoumall.system.service;

import com.xujiu.caigoumall.system.entity.OrderItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 许久龙
* @since 2022-02-21
*/
public interface OrderItemService extends IService<OrderItem> {

}

