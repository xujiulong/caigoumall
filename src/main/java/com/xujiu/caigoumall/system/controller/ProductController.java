package com.xujiu.caigoumall.system.controller;

import com.xujiu.caigoumall.system.service.ProductCommentsService;
import com.xujiu.caigoumall.system.service.ProductService;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
/**
* @author 许久龙
* @since 2022-02-21
*/
@RestController
@CrossOrigin
@RequestMapping("/product")
@Api(value = "提供商品信息相关的接口",tags = "商品管理")
public class ProductController {

    @Autowired
    private ProductService productService;
    @Autowired
    private ProductCommentsService productCommentsService;


    @ApiOperation("查询商品详细信息")
    @GetMapping("/detail-info/{pid}")
    public ResultVO getProductDetailInfo(@PathVariable("pid") String pid){
        ResultVO productDetailInfo = productService.getProductDetailInfo(pid);
        return productDetailInfo;
    }

    @ApiOperation("查询商品参数信息")
    @GetMapping("/detail-params/{pid}")
    public ResultVO getProductParams(@PathVariable("pid") String pid){
        ResultVO productParamsById = productService.getProductParamsById(pid);
        return productParamsById;
    }

    @ApiOperation("查询商品评论信息分页")
    @GetMapping("/detail-commonts/{pid}")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "int",name = "pageNum", value = "当前页码",required = true),
            @ApiImplicitParam(dataType = "int",name = "limit", value = "每页显示条数",required = true)
    })
    public ResultVO getProductCommonts(@PathVariable("pid") String pid,int pageNum,int limit){
        ResultVO resultVO = productCommentsService.listCommontsByProductId(pid, pageNum, limit);
        return resultVO;
    }

    @ApiOperation("查询商品评价统计")
    @GetMapping("/detail-commontscount/{pid}")
    public ResultVO getProductCommontsCount(@PathVariable("pid") String pid){
        ResultVO commentsCountByProductId = productCommentsService.getCommentsCountByProductId(pid);
        return commentsCountByProductId;
    }

    @ApiOperation("根据类别查询商品")
    @GetMapping("/listbycid/{cid}")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "int",name = "pageNum", value = "当前页码",required = true),
            @ApiImplicitParam(dataType = "int",name = "limit", value = "每页显示条数",required = true)
    })
    public ResultVO getProductsByCategoryId(@PathVariable("cid") int cid,int pageNum,int limit){
        return productService.getProductsByCategoryId(cid,pageNum,limit);
    }

    @ApiOperation("根据类别查询商品品牌")
    @GetMapping("/listbrands/{cid}")
    public ResultVO getBrandsByCategoryId(@PathVariable("cid") int cid){
        ResultVO resultVO = productService.getBrandsByCategoryId(cid);
        return resultVO;
    }

    @ApiOperation("根据关键字查询商品")
    @GetMapping("/listbykeyword")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "string",name = "keyword", value = "搜索关键字",required = true),
            @ApiImplicitParam(dataType = "int",name = "pageNum", value = "当前页码",required = true),
            @ApiImplicitParam(dataType = "int",name = "limit", value = "每页显示条数",required = true)
    })
    public ResultVO searchProducts(String keyword,int pageNum,int limit){
        ResultVO resultVO = productService.searchProduct(keyword, pageNum, limit);
        return resultVO;
    }

    @ApiOperation("根据关键字查询商品品牌")
    @GetMapping("/listbrands-keyword")
    @ApiImplicitParam(dataType = "string",name = "keyword", value = "搜索关键字",required = true)
    public ResultVO getBrandsByKeyword(String keyword){
        ResultVO brandsByKeyword = productService.getBrandsByKeyword(keyword);
        return brandsByKeyword;
    }


}
