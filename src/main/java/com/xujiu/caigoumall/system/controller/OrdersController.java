package com.xujiu.caigoumall.system.controller;

import com.xujiu.caigoumall.system.entity.Orders;
import com.xujiu.caigoumall.system.service.OrdersService;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author 许久龙
 * @since 2022-02-21
 */
@RestController
@CrossOrigin
@RequestMapping("/order")
@Api(value = "提供订单相关的操作接口", tags = "订单管理")
public class OrdersController {


    @Autowired
   OrdersService ordersService;

    @PostMapping("/add")
    @ApiOperation("添加订单")
    public ResultVO add(String cids, @RequestBody Orders order) {
        ResultVO resultVO = ordersService.sendToVXPay(cids, order);
        return resultVO;
    }

    @GetMapping("/status/{oid}")
    @ApiOperation("查询订单状态")
    public ResultVO getOrderStatus(@PathVariable("oid") String orderId, @RequestHeader("token") String token) {
        ResultVO resultVO = ordersService.getOrderStatusById(orderId);
        return resultVO;
    }

    @GetMapping("/list")
    @ApiOperation("查询订单分页")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "string", name = "userId", value = "用户ID", required = true),
            @ApiImplicitParam(dataType = "string", name = "status", value = "订单状态", required = false),
            @ApiImplicitParam(dataType = "int", name = "pageNum", value = "页码", required = true),
            @ApiImplicitParam(dataType = "int", name = "limit", value = "每页条数", required = true)
    })
    public ResultVO list(@RequestHeader("token") String token,
                         String userId, String status, int pageNum, int limit) {
        ResultVO resultVO = ordersService.listOrders(userId, status, pageNum, limit);
        return resultVO;
    }
}
