package com.xujiu.caigoumall.system.controller;

import com.xujiu.caigoumall.system.entity.Users;
import com.xujiu.caigoumall.system.service.UsersService;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
/**
* @author 许久龙
* @since 2022-02-21
*/
@RestController
@RequestMapping("/user")
@CrossOrigin
@Api(value = "提供用户的登录和注册接口",tags = "用户管理")
public class UsersController {

    @Autowired
    private UsersService usersService;

    @PostMapping("/login")
    @ApiOperation("登录接口")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "string",name = "username", value = "用户登录账号",required = true),
            @ApiImplicitParam(dataType = "string",name = "password", value = "用户登录密码",required = true)
    })//zhangsan 666666
    public ResultVO login(@RequestBody Users users){
     ResultVO resultVO = usersService.login(users);
     return  resultVO;
   }

    @PostMapping("/regist")
    @ApiOperation("注册接口")
    public ResultVO regist(@RequestBody Users users){
        ResultVO resultVO = usersService.regist(users);
        return  resultVO;
    }

    //点击用户中心时检查 用户是否登录及token是否过期
    @ApiOperation("校验token是否过期")
    @GetMapping("/check")
    public ResultVO userTokencheck(@RequestHeader("token") String token) {
        return new ResultVO(200, "success", null);
    }
}
