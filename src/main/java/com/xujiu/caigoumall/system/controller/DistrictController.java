package com.xujiu.caigoumall.system.controller;

import com.xujiu.caigoumall.system.entity.DictDistrict;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;
import com.xujiu.caigoumall.system.service.DictDistrictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("/districts")
@Api(value = "详细地址操作接口",tags = "详细地址管理")
public class DistrictController {
    @Autowired
    private DictDistrictService dictDistrictService;

    @ApiOperation("查询子地区")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "string",name = "parent", value = "父类id",required = true),
    })
    @GetMapping({""})
    public ResultVO getByParent(String parent) {
        List<DictDistrict> data = dictDistrictService.getByParent(parent);
        return new ResultVO(1,"成功",data);
    }
}
