package com.xujiu.caigoumall.system.controller;

import com.xujiu.caigoumall.system.entity.ShoppingCart;
import com.xujiu.caigoumall.system.service.ShoppingCartService;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
/**
* @author 许久龙
* @since 2022-02-21
*/
@RestController
@RequestMapping("/shopcart")
@CrossOrigin
@Api(value = "提供购物车业务相关接口",tags = "购物车管理")
public class ShoppingCartController {

    @Autowired
    private ShoppingCartService shoppingCartService;

    @GetMapping("/list")
    @ApiOperation("查询购物车通过userId")
    @ApiImplicitParam(dataType = "int",name = "userId", value = "用户ID",required = true)
    public ResultVO list(Integer userId,@RequestHeader("token")String token){
        ResultVO resultVO = shoppingCartService.listShoppingCartsByUserId(userId);
        return resultVO;
    }

    @GetMapping("/listbycids")
    @ApiOperation("查询购物车通过cartId")
    @ApiImplicitParam(dataType = "String",name = "cids", value = "选择的购物车记录的id",required = true)
    public ResultVO listByCids(String cids, @RequestHeader("token")String token){
        ResultVO resultVO = shoppingCartService.listShoppingCartsByCids(cids);
        return resultVO;
    }

    @PostMapping("/add")
    @ApiOperation("添加购物车")
    public ResultVO addShoppingCart(@RequestBody ShoppingCart cart,@RequestHeader("token")String token){
        ResultVO resultVO = shoppingCartService.addShoppingCart(cart);
        return resultVO;
    }

    @PutMapping("/update/{cid}/{cnum}")
    @ApiOperation("修改购物车数量")
    public ResultVO updateNum(@PathVariable("cid") Integer cartId,
                              @PathVariable("cnum") Integer cartNum,
                              @RequestHeader("token") String token){
        ResultVO resultVO = shoppingCartService.updateCartNum(cartId, cartNum);
        return resultVO;
    }


    @DeleteMapping(value = "/delete/{cid}")
    @ApiOperation("删除购物车")
    public ResultVO remove( @PathVariable("cid") String cartIds,
                            @RequestHeader("token") String token) {
        ResultVO resultVO = shoppingCartService.removeByCids(cartIds);
        return resultVO;
    }
}
