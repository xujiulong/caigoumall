package com.xujiu.caigoumall.system.controller;

import com.xujiu.caigoumall.system.entity.vo.CategoryVO;
import com.xujiu.caigoumall.system.service.CategoryService;
import com.xujiu.caigoumall.system.service.IndexImgService;
import com.xujiu.caigoumall.system.service.ProductService;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 许久龙
 * @since 2022-02-21
 */
@RestController
@RequestMapping("/index")
@CrossOrigin
@Api(value = "提供首页数据显示所需的接口", tags = "首页管理")
public class IndexController {

    @Autowired
    private IndexImgService indexImgService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    ProductService productService;

    @GetMapping("/indeximg")
    @ApiOperation("查询首页轮播图")
    public ResultVO getIndexImg() {
        ResultVO resultVO = indexImgService.getIndexImg();
        return resultVO;
    }

    @GetMapping("/category-list")
    @ApiOperation("查询首页分类列表")
    public ResultVO getCategoryList() {
        //查询1级分类下的所有子列表
        List<CategoryVO> categoryVOS = categoryService.listCategories(1);
        if(categoryVOS.size()>0 ){
            return new ResultVO(200,"查询成功",categoryVOS);
        }
        return new ResultVO(401,"查询失败",null);
    }


    @GetMapping("/list-recommends")
    @ApiOperation("查询首页新品推荐")
    public ResultVO getListRecommends() {
        ResultVO resultVO = productService.getListRecommends();
        return resultVO;
    }

    @GetMapping("/category-recommends")
    @ApiOperation("查询首页分类商品推荐")
    public ResultVO getCategoryRecommends(){
        ResultVO resultVO = productService.getCategoryRecommends();
        return resultVO;
    }
}
