package com.xujiu.caigoumall.system.controller;

import com.xujiu.caigoumall.system.entity.UserAddr;
import com.xujiu.caigoumall.system.service.UserAddrService;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
/**
* @author 许久龙
* @since 2022-02-21
*/
@RestController
@CrossOrigin
@Api(value = "提供收货地址相关接口",tags = "收货地址管理")
@RequestMapping("/useraddr")
public class UserAddrController {

    @Autowired
    private UserAddrService userAddrService;

    @ApiOperation("查询地址")
    @GetMapping("/list")
    @ApiImplicitParam(dataType = "int",name = "userId", value = "用户ID",required = true)
    public ResultVO listAddr(Integer userId, @RequestHeader("token") String token){
        ResultVO resultVO = userAddrService.listAddrsByUid(userId);
        return resultVO;
    }



    @ApiOperation("添加地址")
    @PostMapping ("/add_new_address")
    public ResultVO addNewAddress(@RequestBody UserAddr form) {
        // 调用业务对象的方法执行业务
        ResultVO resultVO = userAddrService.addNewAddress(form);
        return resultVO;
    }

    @ApiOperation("根据aid查询地址")
    @GetMapping({"/find"})
    public ResultVO findAddrsByaid(Integer aid) {
        ResultVO resultVO = userAddrService.getByAid(aid);
        return resultVO;
    }

    @ApiOperation("更新地址")
    @PutMapping({"/update"})
    public ResultVO findAddrsByaid(@RequestBody UserAddr address) {
        ResultVO resultVO = userAddrService.updateByAid(address);
        return resultVO;
    }

    @ApiOperation("删除地址")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "int",name = "aid", value = "地址id",required = true),
    })
    @DeleteMapping ("{aid}/delete")
    public ResultVO delete(@PathVariable("aid") Integer aid) {
        ResultVO resultVO = userAddrService.delete(aid);
        return resultVO;
    }

}
