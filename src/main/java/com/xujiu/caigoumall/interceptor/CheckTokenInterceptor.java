package com.xujiu.caigoumall.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xujiu.caigoumall.util.jwtToken;
import com.xujiu.caigoumall.system.entity.vo.ResultVO;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * token拦截器
 */
@Component
public class CheckTokenInterceptor implements HandlerInterceptor {

    @Autowired
    jwtToken jwtToken;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String method = request.getMethod();
        //http第一次 OPTIONS请求 放过
        if("OPTIONS".equalsIgnoreCase(method)){
            return true;
        }
        String token = request.getHeader("token");
        if(token == null){
            ResultVO resultVO = new ResultVO(401, "请先登录！", null);
            doResponse(response,resultVO);
        }else{
            try {
                //验证token（密码正确，有效期内）否则抛出异常
                jwtToken.checkToken(token);
                return true;
            }catch (ExpiredJwtException e){
                ResultVO resultVO = new ResultVO(401, "登录过期，请重新登录！", null);
                doResponse(response,resultVO);
            }catch (UnsupportedJwtException e){
                ResultVO resultVO = new ResultVO(401, "Token不合法，请自重！", null);
                doResponse(response,resultVO);
            }catch (Exception e){
                ResultVO resultVO = new ResultVO(401, "请先登录！", null);
                doResponse(response,resultVO);
            }
        }
        return false;
    }

    private void doResponse(HttpServletResponse response,ResultVO resultVO) throws IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        String s = new ObjectMapper().writeValueAsString(resultVO);
        out.print(s);
        out.flush();
        out.close();
    }

}
