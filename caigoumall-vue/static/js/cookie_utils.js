var operator = "=";

// function getCookieValue(keyStr){
// 	var value = null;
// 	var s = window.document.cookie;
// 	var arr = s.split("; ");
// 	for(var i=0; i<arr.length; i++){
// 		var str = arr[i];
// 		var k = str.split(operator)[0];
// 		var v = str.split(operator)[1];
// 		if(k == keyStr){
// 			value = v;
// 			break;
// 		}
// 	}
// 	return value;
// }

// function setCookieValue(key,value){
// 	document.cookie = key+operator+value;
// }

//向sessionStorage中存储一个JSON对象
function setCookieValue(keyStr, value) {
	sessionStorage.setItem(keyStr, JSON.stringify(value));
}

//从sessionStorage中获取一个JSON对象（取不到时返回null）
 function getCookieValue(keyStr) {
	var str = sessionStorage.getItem(keyStr);
	if (str == '' || str == null || str == 'null' || str == undefined) {
		return null;
	} else {
		return JSON.parse(str);
	}
}
